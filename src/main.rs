use std::error::Error;

use clap::Parser;

mod cli;
use cli::Cli;
use colored::Colorize;
use dialoguer::{console::Term, Select};
use serde::{Deserialize, Serialize};
use serde_json::Value;

const USER_AGENT: &str = "Wik";

#[derive(Debug, Deserialize, Serialize, Clone)]
struct SearchItem {
    title: String,
    pageid: u128,
}

fn main() -> Result<(), Box<dyn Error>> {
    let Cli {
        search_query,
        link,
        all,
        lang,
        browser,
    } = Cli::parse();

    let search_query = search_query.join(" ");
    let query_url = format!("https://{lang}.wikipedia.org/w/api.php?action=query&format=json&redirects=1&formatversion=2&list=search&srsearch={search_query}");
    let user_agent = ureq::AgentBuilder::new().user_agent(USER_AGENT).build();

    let result_items: Vec<SearchItem> = serde_json::from_value(
        user_agent.get(&query_url).call()?.into_json::<Value>()?["query"]["search"].take(),
    )?;

    if result_items.is_empty() {
        println!("No search results for the query: {search_query}");
        return Ok(());
    }

    let selection = Select::new()
        .items(
            &result_items
                .iter()
                .map(|item| item.title.clone())
                .collect::<Vec<String>>(),
        )
        .default(0)
        .clear(true)
        .interact_on_opt(&Term::stdout())?;

    let SearchItem { title, pageid } = match selection {
        Some(index) => &result_items[index],
        None => {
            println!("Nothing was chosen");
            return Ok(());
        }
    };

    let query_url = if all {
        format!("https://{lang}.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&explaintext&redirects=1&pageids={pageid}")
    } else {
        format!("https://{lang}.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro&explaintext&redirects=1&pageids={pageid}")
    };
    let extract: Value = user_agent.get(&query_url).call()?.into_json()?;
    println!(
        "{}",
        dbg!(extract["query"]["pages"][pageid.to_string()]["extract"].as_str()).unwrap()
    );
    if link | browser {
        let title = title.replace(char::is_whitespace, "_");
        let url = format!("https://{lang}.wikipedia.org/wiki/{title}");
        if link {
            println!("{}", format!("\nLink: {url}").italic());
        }
        if browser {
            open::that(url)?;
        }
    }
    Ok(())
}
