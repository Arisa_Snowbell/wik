use clap::Parser;

#[derive(Parser)]
#[clap(version)]
#[clap(author = "Arisa Snowbell - Hyena")]
#[clap(about = "A command line program for getting Wikipedia summaries easily.", long_about = None)]
pub struct Cli {
    #[clap(
        value_parser,
        multiple_occurrences = true,
        use_delimiter = false,
        required = true
    )]
    pub search_query: Vec<String>,
    #[clap(
        long,
        action,
        help = "Print a link to the full article after the summary"
    )]
    pub link: bool,
    #[clap(
        short,
        long,
        action,
        help = "Print all sections of the article (the full page)"
    )]
    pub all: bool,
    #[clap(
        short,
        long,
        help = "Specify language; <LANG> is an HTML ISO language code",
        default_value = "en"
    )]
    pub lang: String,
    #[clap(
        short,
        long,
        action,
        help = "Open full Wikipedia article in default browser"
    )]
    pub browser: bool,
}
